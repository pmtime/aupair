const gulp = require("gulp");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const cssnano = require("gulp-cssnano");
const uglify = require("gulp-uglify");
const useref = require("gulp-useref");
const imagemin = require("gulp-imagemin");
const del = require("del");
const runSequence = require("run-sequence");
const browser = require("browser-sync").create();
const fileinclude = require("gulp-file-include");
const rev = require("gulp-rev");
const revReplace = require("gulp-rev-replace");
const filter = require("gulp-filter");
const svgSprite = require("gulp-svg-sprite");

/* Variables */

var path = {
    src: {
        base: "src",
        html: "src/html",
        sass: "src/sass",
        css: "src/css",
        js: "src/js",
        img: "src/img",
        userdata: "src/userdata",
        favicon: "src/img/favicon",
        fonts: "src/fonts",
    },
    dist: {
        base: "dist",
    },
    sprite: {
        svg: {src: "src/sprite/svg"},
        png: {src: "src/sprite/png"},
    },
};

/* Sprites */

var spriteSrcConfig = {
    dest: ".",
    prefix: ".icon-%s",
    dimensions: "%s",
    sprite: "./img/icons.svg",
    example: {
        template: path.sprite.svg.src + "/icons.html",
        dest: "./icons.html"
    },
    render: {css: {dest: "./css/icons.css"}},
};

var spriteDistConfig = {
    dest: ".",
    prefix: ".icon-%s",
    dimensions: "%s",
    sprite: "./img/icons.svg",
    render: {css: {dest: "./css/icons.css"}},
};

gulp.task("sprite:svg:src", function () {
    gulp.src(path.sprite.svg.src + "/**/*.svg")
        .pipe(svgSprite({
            shape: {id: {separator: "-"}},
            mode: {
                symbol: spriteSrcConfig,
            },
        }))
        .pipe(gulp.dest(path.src.base));
});

gulp.task("sprite:svg:dist", function () {
    gulp.src(path.sprite.svg.src + "/**/*.svg")
        .pipe(svgSprite({
            shape: {id: {separator: "-"}},
            mode: {
                symbol: spriteDistConfig,
            }
        }))
        .pipe(gulp.dest(path.src.base));
});

gulp.task("sprite", ["sprite:svg:src"]);
gulp.task("sprite:dist", ["sprite:svg:dist"]);

/* Browser */

gulp.task("browser", function () {
    browser.init({
        notify: false,
        server: {
            baseDir: path.src.base,
            middleware: [
                {
                    route: "/post/",
                    handle: function (req, res, next) {
                        res.setHeader("Content-Type", "application/json");
                        res.end(JSON.stringify({
                            success: true
                        }));
                    }
                }
            ]
        },
    });
});

/* SASS */

gulp.task("sass", function () {
    return gulp.src([path.src.sass + "/*.sass", path.src.sass + "/*.scss"])
        .pipe(sourcemaps.init())
        .pipe(sass().on("error", sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.src.css))
        .pipe(browser.reload({stream: true}));
});

/* Watch */

gulp.task("watch", ["sprite", "sass", "fileinclude", "browser"], function () {
    gulp.watch([path.src.js + "/**/*.js", path.src.base + "/*.html"], browser.reload);
    gulp.watch([path.src.sass + "/**/*.sass", path.src.sass + "/**/*.scss"], ["sass"]);
    gulp.watch(path.src.html + "/**/*.html", ["fileinclude"]);
    gulp.watch(path.sprite.svg.src + "/**/*.svg", ["sprite:svg:src"]);
});

/* Templates */

gulp.task("fileinclude", function () {
    gulp.src(path.src.html + "/*.html")
        .pipe(fileinclude({
            basepath: "src/html",
            context: {
                sitename: "Au Pair",
            },
        }))
        .pipe(gulp.dest(path.src.base));
});

gulp.task("useref", function () {
    var filterJS = filter("**/*.js", {restore: true});
    var filterCSS = filter("**/*.css", {restore: true});
    var filterHTML = filter(["**/*", "!**/*.html"], {restore: true});

    return gulp.src(path.src.base + "/*.html")
        .pipe(useref())
        .pipe(filterJS)
        .pipe(uglify())
        .pipe(filterJS.restore)
        .pipe(filterCSS)
        .pipe(cssnano())
        .pipe(filterCSS.restore)
        .pipe(filterHTML)
        .pipe(rev())
        .pipe(filterHTML.restore)
        .pipe(revReplace())
        .pipe(gulp.dest(path.dist.base));
});

/* Images */

gulp.task("images", function () {
    return gulp.src([path.src.img + "/**/*", path.src.userdata + "/**/*"], {base: path.src.base})
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
        ]))
        .pipe(gulp.dest(path.dist.base));
});

/* Move */

gulp.task("move:favicon", function () {
    return gulp.src([
            path.src.favicon + "/**/*",
            path.src.base + "/favicon.ico",
            path.src.base + "/manifest.json",
            path.src.base + "/browserconfig.xml",
        ], {base: path.src.base})
        .pipe(gulp.dest(path.dist.base));
});

gulp.task("move:fonts", function () {
    return gulp.src(path.src.fonts + "/**/*", {base: path.src.base})
        .pipe(gulp.dest(path.dist.base));
});

gulp.task("move", ["move:favicon", "move:fonts"]);

/* Clean */

gulp.task("clean:dist", function () {
    return del.sync(path.dist.base, {dot: true});
});

gulp.task("clean:html", function () {
    return del.sync(path.src.base + "/*.html");
});

gulp.task("build", function (callback) {
    runSequence(
        "clean:dist",
        ["sprite:dist", "sass", "fileinclude", "images"],
        "useref",
        "move",
        "clean:html",
        callback
    );
});

gulp.task("default", ["build"]);
