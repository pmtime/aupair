(function ($) {

    $.fn.leveling = function (options) {

        var options = $.extend(true, $.fn.leveling.defaults, options);

        return this.each(function () {

            var container = $(this);
            var items = container.find(options.item);
            var row = calcRow();

            items.each(function () {
                $(this).data("height", $(this).height());
            });

            $(window).resize(function () {
                row = calcRow();
                level();
            });

            level();

            function level() {
                if (row > 1) {
                    var itemsSlice = [];
                    for (var i = 0; i < items.length; i += row) {
                        var itemRow = items.slice(i, i + row);
                        itemsSlice.push(itemRow);
                    }

                    for (var i = itemsSlice.length - 1; i >= 0; i--) {
                        var heights = [];
                        itemsSlice[i].each(function () {
                            heights.push($(this).data("height"));
                        }).height(Math.max.apply(Math, heights));
                    }
                }
            }

            function calcRow() {
                var itemWidth = items.first().outerWidth(true);
                return Math.floor(container.width() / itemWidth);
            }

        });

    };

    $.fn.leveling.defaults = {
        item: "> div"
    };

})(jQuery);
