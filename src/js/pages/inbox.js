(function ($) {

    $("[data-inbox-message]").click(function () {
        $(this).removeClass("unread");
        $("#inbox-messages-list").scrollTop($(".inbox-message-content").prop("scrollHeight") + 200);
        $("#inbox-messages-list").perfectScrollbar("update");

        $("#inbox-content-list").removeClass("active").addClass("hidden");
        $("#inbox-content-message").removeClass("hidden").addClass("active");
    });

    $("[data-inbox-back]").click(function () {
        $("#inbox-content-message").removeClass("active").addClass("hidden");
        $("#inbox-content-list").removeClass("hidden").addClass("active");
    });

})(jQuery);