var listItemHTML = $('.list-item')[0] && $('.list-item')[0].outerHTML;

(function ($) {

    var galleries = $(".carousel-search-image");

    if (galleries.length) {
        galleries.each(function (i) {
            $(this).find("img:not(.slick-cloned)").each(function (j) {
                var angle = project.helpers.getRandom(1, 3) * (j % 2 ? -1 : 1)
                $(this).add($(this).parent().find('[src="' + $(this).attr('src') +'"]'))
                  .addClass(angle > 0 ? 'angle-pos' : 'angle-neg')
                  .css('transform', 'rotate(' + angle + 'deg)');
            });
        });
    }

    $("[data-search-loading]").click(function () {
        $(this).addClass("loading");
    });
    
    $('body').hasClass('page-search') && $(window).scroll(function() {
      if ($('.list-item').length == 0) return
      
      var itemBottomOffset = $('.list-item').last().offset().top + $('.list-item').last().height() + 200
      if ($(window).scrollTop() + $(window).height() >= itemBottomOffset) {
        var $listItemHTML = $(listItemHTML)
        $listItemHTML.find('[data-popup-open]').attr('data-popup-open', 'popup-request' + ($('.list-item').length + 1))
        $listItemHTML.find('[data-popup]').attr('data-popup', 'popup-request' + ($('.list-item').length + 1))
        $listItemHTML.hide().appendTo($('.list')).fadeIn(1000)
        
        var carouselSearchImage = $(".carousel-search-image:not(.slick-initialized)");
        if (carouselSearchImage.length) {
            carouselSearchImage.slick({
                arrows: true,
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                easing: "easeInOutQuart",
                speed: 500,
                prevArrow: carouselPrevArrow,
                nextArrow: carouselNextArrow,
            });
            
            carouselSearchImage.each(function (i) {
                $(this).find("img:not(.slick-cloned)").each(function (j) {
                    var angle = project.helpers.getRandom(1, 3) * (j % 2 ? -1 : 1)
                    $(this).add($(this).parent().find('[src="' + $(this).attr('src') +'"]'))
                      .addClass(angle > 0 ? 'angle-pos' : 'angle-neg')
                      .css('transform', 'rotate(' + angle + 'deg)');
                });
            });
        }
      }
    })

})(jQuery);