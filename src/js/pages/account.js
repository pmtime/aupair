(function ($) {

    var calendar = $("[data-calendar]");

    if (calendar.length) {
        var boxes = $("[data-year]", calendar),
            inputs = $("input[type=checkbox]", calendar),
            yearPrev = 0;

        boxes.each(function () {
            var box = $(this),
                yearNext = parseInt(box.data("year"), 10);

            if (yearNext > yearPrev) {
                box.addClass("highlighted");
                yearPrev = yearNext;
            }
        });
        
        $('.account-calendar-boxes').perfectScrollbar()

        $("[data-calendar-mark]").click(function () {
            inputs.each(function () {
                this.checked = true;
                $(this).trigger("change");
            });
        });

        $("[data-calendar-unmark]").click(function () {
            inputs.each(function () {
                this.checked = false;
                $(this).trigger("change");
            });
        });
    }

    $("[data-text-cut]").each(function () {
        var container = $(this),
            children = $(".list-item", container),
            cut = container.data("text-cut");

        children.each(function () {
            var item = $(this),
                text = $(".list-item-text", item),
                toggle = $(".list-item-text-more", item),
                height = text.height();

            if (height >= cut) {
                text
                    .addClass("cutted")
                    .height(cut)
                    .data("heightBase", height)
                    .data("heightCut", cut);

                toggle.text(toggle.data("text-down"));
            }
        });
    });

    $("[data-text-cut]").on("click", ".list-item-text-more", function () {
        var toggle = $(this),
            target = toggle.prev(),
            cutted = target.hasClass("cutted");

        if (cutted) {
            toggle.hide();
            target.removeClass("cutted").animate({"height": target.data("heightBase")}, 300);
        } else {
            toggle.text(toggle.data("text-down"));
            target.addClass("cutted").animate({"height": target.data("heightCut")}, 300);
        }
    });

})(jQuery);