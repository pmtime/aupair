var project = {

    menu: null,
    modal: null,
    popup: null,

    helpers: {
        /**
         * Случайное число
         * @param min Включая
         * @param max Включая
         * @returns {integer}
         */
        getRandom: function (min, max) {
            return Math.random() * (max - min) + min;
        },

        /**
         * Случайное целое число
         * @param min Включая
         * @param max Включая
         * @returns {integer}
         */
        getRandomInt: function (min, max) {
            return Math.round(Math.random() * (max - min)) + min;
        },

        /**
         * Разбивка числа на разряды (1 000 000 вместо 1000000)
         * @param text Число
         * @returns {string}
         */
        splitDigits: function (text) {
            return text.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        },

        /**
         * Возвращает подходящее для value слово во множественном числе
         * @param {int} value Значение
         * @param {string} text1 Одно значени-Е
         * @param {string} text2 Два значени-Я
         * @param {string} text5 Пять значени-Й
         * @returns {string}
         */
        getRussianPlural: function (value, text1, text2, text5) {
            var text = text1;
            var mod10 = value % 10;

            if (mod10 == 0 || (mod10 >= 5 && mod10 <= 9) || (value >= 11 && value <= 14)) {
                text = text5;
            } else if (mod10 == 1) {
                text = text1;
            } else if (mod10 >= 2 && mod10 <= 4) {
                text = text2;
            }

            return text;
        },

    },

};


$(function () {
    if (typeof svg4everybody !== "undefined") {
        svg4everybody();
    }

    if (typeof lightbox !== "undefined") {
        lightbox.option({
            "albumLabel": "%1/%2",
        });
    }
    
    // Стики хидер.
    if (!$('body').hasClass('page-home') && $('.header .container').children().length > 3) {
      new Headhesive('.header', {
        classes: {
          clone: 'header--clone',
          stick: 'header--stick',
          unstick: 'header--unstick',
        }
      })
    }
    
    $('.header--clone [data-popup-open]').each(function(index, element) {
      $(element).attr('data-popup-open', $(element).attr('data-popup-open') + '-clone')
    })
    
    $('.header--clone [data-popup]').each(function(index, element) {
      $(element).attr('data-popup', $(element).attr('data-popup') + '-clone')
    })
    
    // Попап переключения языка.
    $('.popup-language__wrapper > .popup-language__link').html($('.popup-language .popup-language__link').eq(0).html())
    $('.popup-language__content .popup-language__link').on('click', function() {
      $('.popup-language__wrapper > .popup-language__link').html($(this).html())
      project.popup.close()
      return false
    })
    
    if ($.prototype.textcounter) {
      $('[textcounter]').each(function(index, field) {
        var options = {
          max: $(field).data('textcounter-max'),
          counterText: $(field).data('textcounter-text'),
          countSpaces: true
        }
        
        $(field).textcounter(options)
      })
    }

    if ($.prototype.select2) {
        $("select").select2({
            minimumResultsForSearch: -1,
        });
    }

    if ($.prototype.placeholder) {
        $("input, textarea").placeholder({
            customClass: 'ie-placeholder',
        });
    }

    if ($.prototype.perfectScrollbar) {
        $(".box-scrollbar").perfectScrollbar();
    }

    if ($.prototype.datepick) {

        $(".datepicker-search-from").datepick({
            showAnim: "fadeIn",
            monthsToShow: [12, 1],
            minDate: new Date(),
            maxDate: "+1y",
            dateFormat: "M d, yyyy",
            commandsAsDateFormat: true,
            changeMonth: false,
            useMouseWheel: false,
            firstDay: 1,
            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            renderer: {
                picker: '<div class="datepick">' + '{months}' + '</div>',
                monthRow: '<div class="datepick-month-row">{months}</div>',
                month: '<div class="datepick-month"><div class="datepick-month-header">{monthHeader}</div>' + '<table><tbody>{weeks}</tbody></table></div>',
            },
            onSelect: function (inst, date) {
                setTimeout(function () {
                    inst.elem.find(".datepick").perfectScrollbar();
                }, 100);
                $("[data-datepicker-search-from-text]").text($.datepick.formatDate("M d", date[0])).removeClass('empty');
                $("[data-datepicker-search-from-input]").val($.datepick.formatDate("yyyy-mm-dd", date[0]));
            },
        });

        $(".datepicker-search-to").datepick({
            showAnim: "fadeIn",
            monthsToShow: [12, 1],
            minDate: new Date(),
            maxDate: "+1y",
            dateFormat: "M d, yyyy",
            commandsAsDateFormat: true,
            changeMonth: false,
            useMouseWheel: false,
            firstDay: 1,
            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            renderer: {
                picker: '<div class="datepick">' + '{months}' + '</div>',
                monthRow: '<div class="datepick-month-row">{months}</div>',
                month: '<div class="datepick-month"><div class="datepick-month-header">{monthHeader}</div>' + '<table><tbody>{weeks}</tbody></table></div>',
            },
            onSelect: function (inst, date) {
                setTimeout(function () {
                    inst.elem.find(".datepick").perfectScrollbar();
                }, 100);
                $("[data-datepicker-search-to-text]").text($.datepick.formatDate("M d, yyyy", date[0]));
                $(".popup-datepicker [data-datepicker-search-to-text]").text($.datepick.formatDate("M d", date[0]));
                $("[data-datepicker-search-to-input]").val($.datepick.formatDate("yyyy-mm-dd", date[0]));
            },
        });

        project.datepickers = {
            requestFrom: {
                showAnim: "fadeIn",
                monthsToShow: [12, 1],
                minDate: new Date(),
                maxDate: "+1y",
                dateFormat: "M d, yyyy",
                commandsAsDateFormat: true,
                changeMonth: false,
                useMouseWheel: false,
                firstDay: 1,
                monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                renderer: {
                    picker: '<div class="datepick">' + '{months}' + '</div>',
                    monthRow: '<div class="datepick-month-row">{months}</div>',
                    month: '<div class="datepick-month"><div class="datepick-month-header">{monthHeader}</div>' + '<table><tbody>{weeks}</tbody></table></div>',
                },
                onSelect: function (inst, date) {
                    setTimeout(function () {
                        inst.elem.find(".datepick").perfectScrollbar();
                    }, 100);
                    $("[data-datepicker-request" + inst.elem.data('id') + "-from-text]").text($.datepick.formatDate("M d", date[0]));
                    $("[data-datepicker-request" + inst.elem.data('id') + "-from-input]").val($.datepick.formatDate("yyyy-mm-dd", date[0]));
                },
            },
            requestTo: {
                showAnim: "fadeIn",
                monthsToShow: [12, 1],
                minDate: new Date(),
                maxDate: "+1y",
                dateFormat: "M d, yyyy",
                commandsAsDateFormat: true,
                changeMonth: false,
                useMouseWheel: false,
                firstDay: 1,
                monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                renderer: {
                    picker: '<div class="datepick">' + '{months}' + '</div>',
                    monthRow: '<div class="datepick-month-row">{months}</div>',
                    month: '<div class="datepick-month"><div class="datepick-month-header">{monthHeader}</div>' + '<table><tbody>{weeks}</tbody></table></div>',
                },
                onSelect: function (inst, date) {
                    setTimeout(function () {
                        inst.elem.find(".datepick").perfectScrollbar();
                    }, 100);
                    $("[data-datepicker-request" + inst.elem.data('id') + "-to-text]").text($.datepick.formatDate("M d, yyyy", date[0]));
                    $(".popup-datepicker [data-datepicker-request" + inst.elem.data('id') + "-to-text]").text($.datepick.formatDate("M d", date[0]));
                    $("[data-datepicker-request" + inst.elem.data('id') + "-to-input]").val($.datepick.formatDate("yyyy-mm-dd", date[0]));
                },
            },
        };

        // Удаляем пустую строку в дейтпикерк. Это его баг для января.
        $('.datepick tr').each(function(index, tr) {
          if ($(tr).text().trim() == '') $(tr).remove()
        })
        
        
        // Заполним поле поиска локейшеном для примера.
        $('[name="search"]').val('Boston, AR, United States')
        
        // Удаляем год у текущего месяца в хидере.
        $('.datepick-month-header').each(function(index, header) {
          var currentYear = new Date().getFullYear()
          
          if ($(header).text().indexOf(currentYear) != -1) {
            $(header).text($(header).text().replace(' ' + currentYear, ''))
          }
        })
        
        // Редактирование даты вручную.
        $('[data-datepicker-search-from-text], [data-datepicker-search-to-text]').on('keydown click blur', function(event) {
          var $input = $(event.target)
          
          
          $("[data-datepicker-search-to-text]").attr('contenteditable', true)
          $input.removeClass('empty')
          if ($input.text() == 'Any date') {
            $input.text('')
          } else {
            event.stopPropagation()
          }
          
          if (event.which == 13) event.preventDefault()
          
          // Здесь еще нужно бы распарсить дату и обновить календарь. Удачи ;)
        })
    }
    
    // Прижимаем футер. Вниз. Сильно.
    // Еще я люблю Найлза Купера.
    // https://soundcloud.com/gkokalis/cut-n-paste-w-very-special-guest-niles-cooper-live-on-the-house-of-soundwave-7-21-17
    $('body').append($('.footer').clone().addClass('footer--fixed'))
    $(window).on('resize', function() {
      $('body').toggleClass('footer-fixed', $('body').height() > $('.wrapper').height())
    })
    $(window).trigger('resize')
    
    // Открываем и скрываем дропдаун.
    $('.button-dropdown .toggle').on('click', function() {
      var $button = $(this).closest('.button-dropdown')
      $button.toggleClass('show')
      
      return false
    })
    
    $('.button-dropdown__value').on('click', function() {
      $(this).closest('form').attr('action', $(this).data('action'))
    })
    
    $('.button-dropdown__dropdown').on('mouseleave', function() {
      var $button = $(this).closest('.button-dropdown')
      $button.removeClass('show')
    })
    
    if ($.prototype.autocomplete) {
        $(".autocomplete").autocomplete({
            source: function (request, response) {
                jQuery.getJSON(
                    "http://gd.geobytes.com/AutoCompleteCity?callback=?&q=" + request.term,
                    function (data) {
                        response(data);
                    }
                );
            },
            minLength: 3,
            delay: 100,
        });
    }

});

$(document).ready(function ($) {

    if ($.prototype.mask && $("[data-mask]").length) {
        $("[data-mask=zip]").mask("999999");
        $("[data-mask=phone]").mask("+7 999 999-99-99");
        $("[data-mask=birth]").mask("99/99/9999");
        $("[data-mask=date]").mask("99/99/9999");
    }

});
