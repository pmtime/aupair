/**
 * Плейсхолдеры
 */

(function ($) {

    $("[placeholder]").focus(function () {
        var input = $(this),
            active = input.data("active"),
            container = input.parent(".form-group");

        if (!active) {
            input
                .data("active", true)
                .data("placeholder", input.attr("placeholder"))
                .attr("placeholder", "");

            var placeholder = $("<span/>", {class: "placeholder"})
                .text(input.data("placeholder"))
                .appendTo(container)
                .animate({opacity: 1, top: "-8px"}, 200);

            input.data("placeholder", placeholder);
        }
    });

    $("[placeholder]").blur(function () {
        var input = $(this);

        if (!input.val()) {
            input
                .data("active", false)
                .attr("placeholder", input.data("placeholder").text())
                .data("placeholder").remove();
        }
    });

})(jQuery);