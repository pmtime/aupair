/**
 * Чтобы вы знали о самом важном.
 */

(function ($) {
  window.notify = function(type, html) {
    $('body').append(
      '<div class="notify notify__' + type + '">' +
        '<div class="notify__content">' + html + '</div>' +
      '</div>'
    )
    
    $('.notify').hide().fadeIn(500)
    setTimeout(function() {
      $('.notify').fadeOut(500, function() {
        $(this).remove()
      })
    }, 3000)
  }
})(jQuery);
