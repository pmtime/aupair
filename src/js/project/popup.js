/**
 * Всплывашка
 */

(function ($) {

    var Popup = function () {
        this.active = null;
    }

    Popup.prototype.open = function (target) {
        var popup = $("[data-popup=" + target + "]");
        $('[data-popup-open], [data-popup-toggle]').removeClass('popup-active')
        $('[data-popup]').fadeOut(100).removeClass('active')
        $("[data-popup-open=" + target + "], [data-popup-toggle=" + target + "]").addClass('popup-active')

        popup.addClass("active").stop(true, false).fadeIn(100);
        this.active = true;

        if (popup.hasClass("popup-request")) {
            $(".datepicker-request-from", popup).datepick(project.datepickers.requestFrom);
            $(".datepicker-request-to", popup).datepick(project.datepickers.requestTo);
        }

        if ($.prototype.perfectScrollbar && popup.hasClass("popup-datepicker")) {
            setTimeout(function () {
                $(".datepick").perfectScrollbar();
            }, 500);
        }
    }

    Popup.prototype.close = function () {
        $("[data-popup].active").removeClass("active").stop(true, false).fadeOut(100);
        $("[data-popup-open].popup-active, [data-popup-toggle].popup-active").removeClass('popup-active')
        this.active = false;
    }

    project.popup = new Popup();

    $(document).on("click", "[data-popup-open]", function (e) {
        e.stopPropagation();
        e.preventDefault();
        project.popup.active
          ? project.popup.close()
          : project.popup.open($(this).attr("data-popup-open"))
    });
    
    $(document).on("click", "[data-popup-toggle]", function (e) {
        e.stopPropagation();
        e.preventDefault();
        
        $(this).hasClass('popup-active')
          ? project.popup.close()
          : project.popup.open($(this).attr("data-popup-toggle"));
    });

    $(document).on("click", "[data-popup]", function (e) {
        e.stopPropagation();
    });

    $(document).on("click", "body, [data-popup-close]", function () {
        if (project.popup.active) {
            project.popup.close();
        }
    });

})(jQuery);