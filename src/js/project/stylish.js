/**
 * Стилизованные чекбоксы и радио-кнопки
 */

(function ($) {

    function toggleOnLoad(elem) {
        var checked = elem.find("input").is(":checked");

        if (checked) {
            elem.addClass("checked");
        } else {
            elem.removeClass("checked");
        }
    };

    function toggleOnChange(elem) {
        var checked = elem.is(":checked"),
            label = elem.parent("label"),
            radio = elem.attr("type") == "radio";

        if (radio) {
            $("input[name=" + elem.attr("name") + "]")
                .parent("label")
                .removeClass("checked");
        }

        if (checked) {
            label.addClass("checked");
        } else {
            label.removeClass("checked");
        }
    };

    $(document).on("change", "[data-input-checkbox] input, [data-input-radio] input", function () {
        toggleOnChange($(this));
    });

    $("[data-input-checkbox], [data-input-radio]").each(function () {
        toggleOnLoad($(this));
    });

})(jQuery);