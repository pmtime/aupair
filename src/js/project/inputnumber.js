/**
 * Числовые инпуты
 */

(function ($) {

    var options = {
        min: 0,
        max: 100,
        step: 1,
    }

    function plus() {
        var value = 0,
            control = $(this),
            parent = control.parent(),
            input = parent.find("input"),
            oldval = parseInt(input.val(), 10);

        $(this).closest('.input-number').find('button').removeClass('disabled')
        if (oldval + options.step >= options.max) {
            $(this).closest('.input-number').find('button.plus').addClass('disabled')
            value = options.max;
        } else {
            value = oldval + options.step;
        }

        input.val(value);

        return false;
    }

    function minus() {
        var value = 0,
            control = $(this),
            input = control.parent().find("input"),
            oldval = parseInt(input.val(), 10);

        $(this).closest('.input-number').find('button').removeClass('disabled')
        if (oldval - options.step <= options.min) {
            $(this).closest('.input-number').find('button.minus').addClass('disabled')
            value = options.min;
        } else {
            value = oldval - options.step;
        }

        input.val(value);

        return false;
    }

    $(".input-number").each(function () {
        var self = $(this);

        var buttonPlus = $("<button/>", {
            "class": "button input-number-control plus",
            "html": "+",
        }).on("click", plus);

        var buttonMinus = $("<button/>", {
            "class": "button input-number-control minus disabled",
            "html": "&minus;",
        }).on("click", minus);

        self.append(buttonPlus);
        self.prepend(buttonMinus);
    });

})(jQuery);