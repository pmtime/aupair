/**
 * Формы
 */

(function ($) {

    $("#form-signin").validate({
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#form-signup-aupair-step1").validate({
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#form-signup-aupair-step2").validate({
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#form-signup-host-step1").validate({
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#form-signup-host-step2").validate({
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#form-signup-host-step3").validate({
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#form-signup-host-step4").validate({
        submitHandler: function (form) {
            form.submit();
        }
    });

    if (typeof Dropzone !== "undefined") {

        Dropzone.autoDiscover = false;

        $("#dropzone-avatar-form").dropzone({
            url: "/post/",
            maxFilesize: 100,
            maxFiles: 1,
            thumbnailWidth: 160,
            thumbnailHeight: 110,
            acceptedFiles: "image/*",
            previewsContainer: "#dropzone-avatar-form-preview",
            previewTemplate: $("#dropzone-avatar-preview-template").html(),
            init: function () {
                this.on("maxfilesexceeded", function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
            },
        });

        $("#dropzone-avatar-card").dropzone({
            url: "/post/",
            maxFilesize: 100,
            maxFiles: 1,
            thumbnailWidth: 130,
            thumbnailHeight: 130,
            acceptedFiles: "image/*",
            previewsContainer: false,
            init: function () {
                this.on("maxfilesexceeded", function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
                this.on("thumbnail", function (file, dataUrl) {
                    $("#dropzone-avatar-card-preview").attr("src", dataUrl);
                    $('.account-profile-image-empty').removeClass('account-profile-image-empty')
                });
            },
        });

        $("#dropzone-gallery-form").dropzone({
            init: function() {
              this.on("addedfile", function() {
                // Позволяем сабмитить форму.
                $('.dropzone-gallery-form')
                  .parent()
                  .find('.form-group-wrapper-submit button')
                  .removeAttr('disabled')
                
                // Помечаем первый файл как кавер.
                if ($('.dropzone-previews').children().length == 1) {
                  $('.dropzone-previews .input-checkbox').addClass('checked')
                }
              })
            },
            url: "/post/",
            maxFilesize: 100,
            thumbnailWidth: null,
            thumbnailHeight: 240,
            acceptedFiles: "image/*",
            previewsContainer: "#dropzone-gallery-form-preview",
            previewTemplate: $("#dropzone-gallery-preview-template").html(),
            addRemoveLinks: true,
        });

        $("#dropzone-inbox-form").dropzone({
            url: "/post/",
            maxFilesize: 100,
            createImageThumbnails: false,
            previewsContainer: "#inbox-messages-list",
            previewTemplate: $("#dropzone-inbox-preview-template").html(),
            init: function () {
                this.on("success", function (file) {
                    $("#inbox-messages-list").scrollTop($(".inbox-message-content").prop("scrollHeight") + 200);
                });
            },
        });

    }

})(jQuery);