/**
 * Стилизованные файловые инпуты
 */

(function ($) {

    var fileContents = $("#inputfile-content");

    $(document).on('change', '.inputfile', function (e) {
        var input = $(this).find("input:visible"),
            html = fileContents.html(),
            index = parseInt(input.data("index"), 10),
            filename = e.target.value.split("\\").pop() || "",
            //fileinfo = '<div>' + filename + ' <span class="link link-pseudo inputfile-remove" data-index="' + index + '">удалить</span></div>',
            fileinfo = '<div>' + filename + ' </div>',
            clone = input.clone().hide();

        html += fileinfo;

        clone
            .attr("data-index", index + 1)
            .attr("name", "files[" + (index + 1) + "]")
            .insertAfter(input)
            .show();

        input.hide();
        fileContents.html(html);
    });


    /*$(document).on("click", ".inputfile-remove", function (e) {
     e.preventDefault();

     var index = parseInt($(this).data("index"), 10);

     $("input[data-index=" + index + "]").remove();
     $(this).parent("div").remove();
     });*/

})(jQuery);