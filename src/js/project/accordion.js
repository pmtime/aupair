/**
 * Аккордион
 */

(function ($) {

    var options = {
        speed: 400,
        easing: "easeInOutCubic",
    };

    $("[data-accordion]").each(function () {
        var self = $(this),
            active = $("[data-accordion-toggle=" + self.data('accordion') + "]").hasClass("active");

        if (!active) {
            self.hide();
        }
    });

    $("[data-accordion-toggle]").click(function () {
        var toggle = $(this),
            target = $("[data-accordion=" + toggle.data("accordionToggle") + "]"),
            active = toggle.hasClass("active");

        target.slideToggle(options.speed, options.easing);
        toggle.toggleClass("active");
    });

})(jQuery);