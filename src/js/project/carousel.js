/**
 * Карусель
 */

(function ($) {
  setTimeout(function() {

    var carouselSearchImage = $(".carousel-search-image");

    if (carouselSearchImage.length) {
        carouselSearchImage.slick({
            arrows: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            easing: "easeInOutQuart",
            speed: 500,
            prevArrow: carouselPrevArrow,
            nextArrow: carouselNextArrow,
        });
    }

    var carouselProfileGallery = $("#carousel-profile-gallery");

    if (carouselProfileGallery.length) {
        carouselProfileGallery.slick({
            arrows: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            easing: "easeInOutQuart",
            speed: 500,
            prevArrow: carouselPrevArrow,
            nextArrow: carouselNextArrow,
        });
    }

  }, 100)
})(jQuery);