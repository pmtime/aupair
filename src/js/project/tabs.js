/* Табы */

$(function () {

    var controls = $("[data-tab-toggle]"),
        contents = $("[data-tab]");

    function toggle(name) {
        var toggle = $("[data-tab-toggle=" + name + "]"),
            target = $("[data-tab=" + name + "]");

        controls.removeClass("active");
        toggle.addClass("active");

        contents.stop(true, false).hide();
        target.stop(true, false).fadeIn(1000);
    }

    if (controls.length) {
        $("[data-tab-toggle]").click(function () {
            toggle($(this).data("tab-toggle"));
        });
    }

});