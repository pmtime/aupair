/**
 * Модальное окно
 */

(function ($) {

    var Modal = function () {
        this.originalBodyPad = null;
        this.active = false;
        this.scrollbarWidth = 0;
    }

    Modal.prototype.checkScrollbar = function () {
        var fullWindowWidth = window.innerWidth;
        this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth;
        this.scrollbarWidth = this.measureScrollbar();
    }

    Modal.prototype.setScrollbar = function () {
        var bodyPad = parseInt(($("body").css("padding-right") || 0), 10);
        this.originalBodyPad = document.body.style.paddingRight || "";
        if (this.bodyIsOverflowing) {
            $("body").css("padding-right", bodyPad + this.scrollbarWidth);
        }
    }

    Modal.prototype.measureScrollbar = function () {
        var scrollDiv = document.createElement("div");
        scrollDiv.className = "modal-scrollbar-measure";
        $("body").append(scrollDiv);
        var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        document.body.removeChild(scrollDiv);
        return scrollbarWidth;
    }

    Modal.prototype.open = function (target) {
        if (this.active) {
            this.close();
        }
        var modal = $("[data-modal=" + target + "]"),
            content = modal.find(".modal-content");

        this.checkScrollbar();
        this.setScrollbar();
        $("body").addClass("modal-open");

        modal.addClass("active").fadeIn(300, function () {
            content.addClass("active");
        });

        this.active = true;
    }

    Modal.prototype.close = function () {
        var modal = $("[data-modal].active"),
            content = modal.find(".modal-content");

        content.removeClass("active");
        modal.removeClass("active");
        modal.fadeOut(500, function () {
            $("body")
                .removeClass("modal-open")
                .css("padding-right", "");
        });

        this.active = false;
    }

    project.modal = new Modal();

    $("[data-modal-open]").click(function (e) {
        e.preventDefault();
        project.modal.open($(this).attr("data-modal-open"));
    });

    $("[data-modal], [data-modal-close]").click(project.modal.close);

    $(".modal-content").click(function (e) {
        e.stopPropagation();
    });

})(jQuery);