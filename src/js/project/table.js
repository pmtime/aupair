/**
 * Адаптивная таблица
 */

(function ($) {

    var tables = $("table.responsive");

    if (tables.length) {
        tables.each(function () {
            var table = $(this),
                thead = table.find("thead th"),
                tbody = table.find("tbody tr");

            tbody.each(function (i) {
                $(this).find("td").each(function (j) {
                    $(this).attr("data-heading", thead.eq(j).text());
                });
            });
        });
    }

})(jQuery);