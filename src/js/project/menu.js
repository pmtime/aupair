/**
 * Переключатель меню
 */

(function ($) {

    var Menu = function () {
        this.$activeMenu = null;
        this.$activeToggle = null;
        this.active = null;

        this.bind();
    }

    Menu.prototype.bind = function () {
        var self = this;
        $("[data-menu-toggle]").on("click", function () {
            self.open($(this).attr("data-menu-toggle"));
        });
    }

    Menu.prototype.calculateOffset = function () {
        return $(window).width() - this.$activeToggle.offset().left - this.$activeToggle.outerWidth() + 1;
    }

    Menu.prototype.setOffset = function () {
        this.$activeMenu.css("right", this.calculateOffset() + "px");
    }

    Menu.prototype.open = function (target) {
        if (this.active == target) {
            this.close();
        } else {
            this.close();

            this.$activeMenu = $("[data-menu=" + target + "]");
            this.$activeToggle = $("[data-menu-toggle=" + target + "]");
            this.active = target;

            this.setOffset();

            this.$activeMenu.addClass("active");
            this.$activeToggle.addClass("active");
        }
    }

    Menu.prototype.close = function () {
        if (this.active) {
            this.$activeMenu.removeClass("active");
            this.$activeToggle.removeClass("active");
            this.$activeMenu = null;
            this.$activeToggle = null;
            this.active = null;
        }
    }

    project.menu = new Menu();

    $("body").click(function () {
        if (project.menu.active) {
            project.menu.close();
        }
    });

    $("[data-menu], [data-menu-toggle]").click(function (e) {
        e.stopPropagation();
    });

})(jQuery);